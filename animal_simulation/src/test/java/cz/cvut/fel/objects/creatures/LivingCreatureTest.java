package cz.cvut.fel.objects.creatures;

import cz.cvut.fel.workspace.distantlocation.SimLocation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LivingCreatureTest {

    LivingCreature creature;
//    @Mock
    SimLocation simLocation;

    @BeforeEach
    void setup() {
//        simLocation = Mockito.mock(SimLocation.class);
        creature = new Bear(0, 0, 1, 5, 3, simLocation, 5);
    }

    @Test
    void newDayUpdateEnergyGoesDown_test() {
        int currEnergy = creature.energy;
        creature.newDayUpdate();
        assertEquals(currEnergy - 1, creature.energy);
    }

    @Test
    void newDayUpdateDaysOldGoesUp_test() {
        int currDaysOld = creature.daysOld;
        creature.newDayUpdate();
        assertEquals(currDaysOld + 1, creature.daysOld);
    }

    @Test
    void newDayUpdateMatedThisDayIsFalse_test() {
        creature.matedThisRound = true;
        creature.newDayUpdate();
        assertEquals(false, creature.matedThisRound);
    }

//    @Test
//    void newDayUpdateWhenTooOldDie_test(){
//        creature.daysOld = creature.lifeLength;
//        creature.newDayUpdate();
//        Mockito.verify(simLocation).updateObjectField(Mockito.anyInt(), Mockito.anyInt(), null);
//    }

//    @Test
//    void newDayUpdateIfEnergyIsZeroItDies_test(){
//        creature.energy = 0;
//        creature.newDayUpdate();
//        Mockito.verify(simLocation).updateObjectField(Mockito.anyInt(), Mockito.anyInt(), null);
//    }
//    @Test
//    void eatEnergyDoesNotRiseOverMaxEnergy_test(){
//        Mockito.when(simLocation.getNeighbors(Mockito.anyInt(), Mockito.anyInt())).thenReturn(
//                {Identificator.FOX, null, null, null});
//        Mockito.when(simLocation.getObject(Mockito.anyInt(), Mockito.anyInt())).thenReturn(new Fox(0,0,0,1,1,simLocation, 1));
//        creature.energy = creature.getMaxEnergy()-1;
//        creature.eat();
//        assertEquals(creature.energy, creature.getMaxEnergy());
//    }

//    @Test
//    void eatCreatureDoesNotEatObjectsThatAreNotOnTheList_test() {
//        Mockito.when(simLocation.getObject(Mockito.anyInt(), Mockito.anyInt())).thenReturn(new Fox(0,0,0,1,1,simLocation, 1));
//        creature.energy -=2; //set so that the energy is not maxed out
//        int currEnergy = creature.energy;
//        creature.eat();
//        assertEquals(creature.energy, currEnergy);
//    }

//    @Test
//    void eatCreatureDoesNotEatWhenEnergyIsFull_test(){
//        Mockito.when(simLocation.getObject(Mockito.anyInt(), Mockito.anyInt())).thenReturn(new Fox(0,0,0,1,1,simLocation, 1));
//        creature.energy = creature.getMaxEnergy(); //making sure the energy is maxed out
//        Mockito.verify(simLocation).updateObjectField(Mockito.anyInt(), Mockito.anyInt(), null);
//    }

//    @Test
//    void moveDoesNotMoveOnXAxisIfAllFieldsAreFull_test(){
//        Mockito.when(simLocation.getObject(Mockito.anyInt(), Mockito.anyInt())).thenReturn(new Barrier(new Location(0,0, true), simLocation, Identificator.ROCK));
//        creature.move();
//        assertEquals(creature.getLocation().getCoordX(), 0)( creature.getLocation().getCoordY() == 0);
//
//    }

//    @Test
//    void moveDoesNotMoveOnYAxisIfAllFieldsAreFull_test(){
//        Mockito.when(simLocation.getObject(Mockito.anyInt(), Mockito.anyInt())).thenReturn(new Barrier(new Location(0,0, true), simLocation, Identificator.ROCK));
//        creature.move();
//        assertEquals( creature.getLocation().getCoordY(), 0);
//
//    }
}