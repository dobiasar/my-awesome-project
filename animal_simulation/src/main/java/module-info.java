module animal.simulation {
    requires java.logging;
    requires java.desktop;
    requires com.fasterxml.jackson.databind;
}