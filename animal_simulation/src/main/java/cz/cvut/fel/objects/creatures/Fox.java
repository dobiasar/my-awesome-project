package cz.cvut.fel.objects.creatures;

import cz.cvut.fel.objects.Terrain;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

import java.util.HashMap;

/**
 * subclass of {@link Animal}
 * represent foxes
 */
public class Fox extends Animal {
    public Fox(int coordX, int coordY, int fieldsPerRound, int maxEnergy, int lifeLength, SimLocation simLocation, int multiplicationRate) {
        super(coordX, coordY,  fieldsPerRound, maxEnergy, lifeLength,Identificator.FOX,  simLocation, multiplicationRate);
        this.possibleTerrain = new Terrain[]{Terrain.FOREST, Terrain.MEADOW};

        this.possibleFood = new HashMap<>();
        possibleFood.put(Identificator.BIRD, maxEnergy / 2);
        possibleFood.put(Identificator.HARE, maxEnergy);
    }
}
