package cz.cvut.fel.objects;

import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.objects.utilities.Location;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

/**
 * class to represent static untimed objects that are not suitable for eating
 */
public class Barrier extends GeneralObject{
    private final Location location;
    private final SimLocation simLocation;
    private final Identificator identificator;

    public Barrier(Location location, SimLocation simLocation, Identificator identificator) {
        super(identificator, location, simLocation);
        this.location = location;
        this.simLocation = simLocation;
        this.identificator = identificator;

    }

    @Override
    public Identificator getIdentificator(){
        return identificator;
    }

}
