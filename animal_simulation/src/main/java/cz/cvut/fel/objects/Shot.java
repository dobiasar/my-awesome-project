package cz.cvut.fel.objects;

import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.objects.utilities.Location;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

/**
 * class to represent hunter's arrow
 * it can kill animals
 */
public class Shot extends GeneralObject implements Timed {
    private final int[] direction;
    private static final int MAX_ROUNDS = 5;
    private int currentRounds = 0;

    public Shot(int coordX, int coordY, int[] direction, SimLocation simLocation) {
        super(Identificator.SHOT, new Location(coordX, coordY, false), simLocation);
        this.direction = direction;
    }

    /**
     * move shot onto new spot
     */
    @Override
    public void newDayUpdate() {
        // Remove shot from current field in objectField
        simLocation.updateObjectField(location.getCoordX(), location.getCoordY(), null);

        // If shot hasn't reached its max rounds do the following actions
        if (currentRounds != MAX_ROUNDS) {
            currentRounds++;
            // Move shot in given direction if its not the end of simLocation
            if (location.getCoordX() + (direction[0]) >= 0 && location.getCoordX() + (direction[0]) < simLocation.getWidth() && (location).getCoordY() + (direction[1]) >= 0 && (location).getCoordY() + (direction[1]) < simLocation.getHeight()) {
                location.setCoordX(location.getCoordX() + (direction[0]));
                location.setCoordY(location.getCoordY() + (direction[1]));
                resultsOfShotMove();
            }
        }
    }

    /**
     * decide weather update of the shot had any affect on the surroundings
     */
    public void resultsOfShotMove() {
        // If there is nothing on the new position, move the shot there
        if (simLocation.getObject(location.getCoordX(), location.getCoordY()) == null) {
            simLocation.updateObjectField(location.getCoordX(), location.getCoordY(), this);
            //Else if there is an animal "kill it" (remove it from to objectField)
        } else if (simLocation.getObject(location.getCoordX(), location.getCoordY()).getIdentificator().getKind() == Identificator.Kind.ANIMAL) {
            simLocation.updateObjectField(location.getCoordX(), location.getCoordY(), null);
        }
        // All other options (barrier, food, human) deactivate the shot, so it's not necessary to do anything (shot has already been removed from previous position)

    }

    public int[] getDirection() {
        return direction;
    }
}
