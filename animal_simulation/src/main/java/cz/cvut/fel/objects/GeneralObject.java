package cz.cvut.fel.objects;

import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.objects.utilities.Location;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

/**
 * abstract class to define parameters needed for all objects on the map
 *
 * @see Barrier
 * @see Shot
 * @see cz.cvut.fel.objects.creatures.LivingCreature
 * @see cz.cvut.fel.objects.foods.Food
 */
public abstract class GeneralObject {
    protected final Identificator identificator;
    protected final Location location;
    protected  final SimLocation simLocation;

    protected GeneralObject(Identificator identificator, Location location, SimLocation simLocation) {
        this.identificator = identificator;
        this.location = location;
        this.simLocation = simLocation;
    }

    public Identificator getIdentificator() {
        return identificator;
    }

    public Location getLocation() {
        return location;
    }

    public SimLocation getSimLocation() {
        return simLocation;
    }

}
