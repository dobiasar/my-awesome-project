package cz.cvut.fel.objects.creatures;

import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

import java.io.IOException;

import static java.lang.Math.max;

/**
 * subclass of {@link LivingCreature}
 * class to represent all animals in the simulation
 *
 * @see Bear
 * @see Bird
 * @see Fish
 * @see Fox
 * @see Hare
 */
public class Animal extends LivingCreature {
    int multiplicationRate;

    public Animal(int coordX, int coordY, int fieldsPerRound, int maxEnergy, int lifeLength, Identificator identificator, SimLocation simLocation, int multiplicationRate) {
        super(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, identificator, simLocation);
        this.multiplicationRate = multiplicationRate;
    }

    /**
     * if animal has more than half of its energy full, it is able to produce new instances
     */
    public void mate() {
        if ((energy >= maxEnergy / 2) && !matedThisRound) {
            // Animals can mate only if they have at least half of max energy

            // Get all neighbors
            Identificator[] neighbors = simLocation.getNeighbors(location.getCoordX(), location.getCoordY());
            int currCoordX = location.getCoordX();
            int currCoordY = location.getCoordY();
            for (int i = 0; i < 4; i++) {
                // If (neighboring field is within this simLocation)
                // && neighbor is the same species as this animal (identificators)
                // && animal hasn't mated this round
                // && neighbor animal has enough energy
                if (currCoordX + directions[i][0] >= 0 && currCoordX + directions[i][0] < simLocation.getWidth()
                        && currCoordY + directions[i][1] >= 0 && currCoordY + directions[i][1] < simLocation.getHeight()
                        && (neighbors[i] == identificator) && !((Animal) simLocation.getObject(currCoordX
                        + directions[i][0], currCoordY + directions[i][1])).hasMatedThisRound() &&
                        (((Animal) simLocation.getObject(currCoordX + directions[i][0], currCoordY
                                + directions[i][1])).getEnergy() >= ((Animal) simLocation.getObject(currCoordX +
                                directions[i][0], currCoordY + directions[i][1])).getMaxEnergy() /2 )) {

                    // Change MatedThisRound variable to true for both animals
                    ((Animal) simLocation.getObject(location.getCoordX() + directions[i][0],
                            location.getCoordY() + directions[i][1])).setMatedThisRound(true);
                    matedThisRound = true;
                    findPlaceForNewAnimals();
                }
            }
        }
    }

    /**
     * algorithm for finding space for new instance
     */
    protected void findPlaceForNewAnimals() {
        // Generate amount of new animals given by multiplication rate
        int madeAnimals = 0;
        for (int j = 0; j < 2; ++j) {
            // Checks every field of current simLocation if no space is found, animal doesn't mate this round
            for (int[] direction : directions) {
                if (isLocationPossible(direction, j)) {
                    // If given location is possible make new instance of the same species
                    newAnimalInstance(location.getCoordX() + (direction[0] * j), location.getCoordY() + direction[1] * j);
                    madeAnimals++;
                }

                // If maximum amount of animals has been made, return
                if (madeAnimals == multiplicationRate) {
                    return;
                }
            }
        }
    }

    /**
     * create new instance of animal of the same kind
     *
     * @param coordX location of new instance
     * @param coordY location of new instance
     */
    protected void newAnimalInstance(int coordX, int coordY) {
        // Make new animal instance on given field according to which identificator has current animal
        switch (identificator) {
            case BEAR -> {
                Bear bear = new Bear(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, simLocation, multiplicationRate);
                simLocation.updateObjectField(coordX, coordY, bear);
            }
            case FOX -> {
                Fox fox = new Fox(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, simLocation, multiplicationRate);
                simLocation.updateObjectField(coordX, coordY, fox);
            }
            case HARE -> {
                Hare hare = new Hare(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, simLocation, multiplicationRate);
                simLocation.updateObjectField(coordX, coordY, hare);
            }
            case BIRD -> {
                Bird bird = new Bird(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, simLocation, multiplicationRate);
                simLocation.updateObjectField(coordX, coordY, bird);
            }
            case FISH -> {
                Fish fish = new Fish(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, simLocation, multiplicationRate);
                simLocation.updateObjectField(coordX, coordY, fish);
            }
            default -> {
                //is empty
            }
        }
    }
}
