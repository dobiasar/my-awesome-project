package cz.cvut.fel.objects.foods;

import cz.cvut.fel.objects.GeneralObject;
import cz.cvut.fel.objects.Timed;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.objects.utilities.Location;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

/**
 * subclass of {@link GeneralObject}
 * implements interface {@link Timed}
 * represent edible objects
 */
public class Food extends GeneralObject implements Timed {
    private final int respawnRate;
    private boolean isRiped;
    private int daysTillRipening;

    public Food(int coordX, int coordY, int respawnRate, SimLocation simLocation, Identificator identificator) {
        super(identificator, new Location(coordX, coordY, true), simLocation);
        this.respawnRate = respawnRate;
        this.daysTillRipening = respawnRate;
    }

    /**
     * after certain timespan food ripens and is ready to be eaten by animal/human
     */
    @Override
    public void newDayUpdate() {
        if (!isRiped && daysTillRipening == 0) {
            isRiped = true;
        } else {
            daysTillRipening -= 1;
        }
    }

    public boolean isRiped() {
        return isRiped;
    }
    
    public void eatFood() {
        this.daysTillRipening = respawnRate;
        isRiped = false;
    }

    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public SimLocation getSimLocation() {
        return simLocation;
    }

    @Override
    public Identificator getIdentificator() {
        return identificator;
    }
}
