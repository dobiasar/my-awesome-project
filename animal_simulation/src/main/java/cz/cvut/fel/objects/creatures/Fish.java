package cz.cvut.fel.objects.creatures;

import cz.cvut.fel.objects.Terrain;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

/**
 * subclass of {@link Animal}
 * represent fish
 * fish have special conditions for mating and dying
 */
public class Fish extends Animal {

    public Fish(int coordX, int coordY, int fieldsPerRound, int maxEnergy, int lifeLength, SimLocation simLocation, int multiplicationRate) {
        super(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, Identificator.FISH, simLocation, multiplicationRate);
        this.possibleTerrain = new Terrain[] {Terrain.WATER};
    }

    /**
     * every 4th day fish multiply regardless of the presence of other instance of their kind
     */
    @Override
    public void mate() {
        if (daysOld % 7 == 0) {
            findPlaceForNewAnimals();
            }
        }

    /**
     * fish and special type of animal, they do not need to eat therefore this method is empty
     */
    @Override
    public void eat(){
        //fish and special type of animal, they do not need to eat therefore this method is empty
    }
}
