package cz.cvut.fel.objects.utilities;

/**
 * class for representing location and ability to move of {@link cz.cvut.fel.objects.GeneralObject}
 */
public class Location {
    private int coordX;
    private int coordY;
    private final boolean isStatic;

    /**
     *
     * @param coordX current coordinate of object
     * @param coordY current coordinate of object
     * @param isStatic represents objects ability to move (if isStatic is true, object cannot move)
     */
    public Location(int coordX, int coordY, boolean isStatic )
    {
        this. coordX = coordX;
        this.coordY = coordY;
        this.isStatic = isStatic;
    }

    public int getCoordX() {
        return coordX;
    }

    public void setCoordX(int coordX) throws IndexOutOfBoundsException{
        if(!isStatic) {
            this.coordX = coordX;
        }
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordY(int coordY) throws IndexOutOfBoundsException{
        if(!isStatic) {
            this.coordY = coordY;
        }
    }

    public boolean isStatic() {
        return isStatic;
    }
}
