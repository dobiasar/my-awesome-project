package cz.cvut.fel.objects;

/**
 * enum with possible types of terrain
 */
public enum Terrain {
    MEADOW(0),
    FOREST(1),
    WATER(2);

    private int number;

    Terrain(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
