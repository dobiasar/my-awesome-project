package cz.cvut.fel.objects.creatures;


import cz.cvut.fel.objects.GeneralObject;
import cz.cvut.fel.objects.Terrain;
import cz.cvut.fel.objects.Timed;
import cz.cvut.fel.objects.foods.Food;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.objects.utilities.Location;
import cz.cvut.fel.workspace.distantlocation.SimLocation;
import cz.cvut.fel.workspace.network.Message;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

/**
 * subclass of {@link GeneralObject}
 * implements interface {@link Timed}
 * <p>
 * class to represent all living (non-static) creatures
 *
 * @see Animal
 * @see Hunter
 */
public class LivingCreature extends GeneralObject implements Timed {

    protected final Random randomGenerator = new Random();

    protected final int maxEnergy;
    protected final int lifeLength;
    protected final int fieldsPerRound;

    protected Terrain[] possibleTerrain;
    protected Map<Identificator, Integer> possibleFood;// maybe map with energy values?? but definitely enum
    protected int[][] possibleMoves;
    protected final int[][] directions = {{0, -1}, {-1, 0}, {1, 0}, {0, 1}};

    protected int daysOld = 0;
    protected boolean matedThisRound = false;
    protected int energy;

    public LivingCreature(int coordX, int coordY, int fieldsPerRound, int maxEnergy, int lifeLength, Identificator identificator, SimLocation simLocation) {
        super(identificator, new Location(coordX, coordY, false), simLocation);
        this.fieldsPerRound = fieldsPerRound;
        this.maxEnergy = maxEnergy;
        this.energy = maxEnergy;
        this.lifeLength = lifeLength;
    }

    /**
     * delete object if it had died
     * update energy, age and mating status
     */
    @Override
    public void newDayUpdate() {
        if (energy == 0 || daysOld == lifeLength) {
            // If creature has zero energy or reaches its lifespan delete it from objectField
            simLocation.updateObjectField(location.getCoordX(), location.getCoordY(), null);
        } else {
            energy -= 1;
            daysOld += 1;
            matedThisRound = false;
        }
    }

    /**
     * decide which field is the creature able to move to
     */
    public void move() {
        boolean[] triedMoves = new boolean[directions.length];
        for (int i = 0; i < directions.length; i++) {
            triedMoves[i] = false;
        }
        boolean hasMoved = false;
        while (!hasMoved) {
            boolean didNotTryAll = false;
            //check if we hadn't tried all possible moves
            for (boolean statement : triedMoves) {
                if (!statement) {
                    didNotTryAll = true;
                    break;
                }
            }
            if (!didNotTryAll) {
                break;
            }
            int idx = randomGenerator.nextInt(directions.length);
            if (isLocationPossible(directions[idx], fieldsPerRound)) {
                simLocation.updateObjectField(location.getCoordX(), location.getCoordY(), null);
                location.setCoordX(location.getCoordX() + (directions[idx][0] * fieldsPerRound));
                location.setCoordY(location.getCoordY() + (directions[idx][1] * fieldsPerRound));
                simLocation.updateObjectField(location.getCoordX(), location.getCoordY(), this);
                hasMoved = true;
            } else {
                triedMoves[idx] = true;
            }
        }
    }

    protected boolean isLocationPossible(int[] direction, int radius) {
        boolean ret = false;
        if (location.getCoordX() + (direction[0] * radius) >= 0 && location.getCoordX() + (direction[0] * radius)
                < simLocation.getWidth() && location.getCoordY() + (direction[1] * radius) >= 0 && location.getCoordY()
                + (direction[1] * radius) < simLocation.getHeight() && simLocation.getObject(location.getCoordX()
                + (direction[0] * radius), location.getCoordY() + (direction[1] * radius)) == null) {
            Terrain terrain = simLocation.getGameMap()[location.getCoordX() + (direction[0] * radius)][location.getCoordY() + (direction[1] * radius)];
            for (Terrain terr : possibleTerrain) {
                if (terr == terrain) {
                    ret = true;
                    break;
                }
            }
        }
        return ret;
    }

    /**
     * Check for edible creatures/{@link Food} on neighboring fields
     * replenish energy levels if possible
     *
     * @throws IOException if instance is located near simulation location edges, it might face communication issues
     */
    public void eat() {
        // get list of all neighboring animals/plants
        Identificator[] neighbors = simLocation.getNeighbors(location.getCoordX(), location.getCoordY());

        // go through list of neighbors
        for (int i = 0; i < 4; i++) {
            // if there is no animal, skip to another round
            if (neighbors[i] != null) {
                // is neighbor in possible food?
                if (possibleFood.containsKey(neighbors[i])) {
                    // is in current simLocation?
                    if (isInBounds(i)) {
                        if (neighbors[i].getKind() == Identificator.Kind.FOOD) {
                            // Neighbor is food, check if its riped, if so, set plant to isRiped = false (eatFood())
                            Food food = (Food) simLocation.getObject(location.getCoordX() + directions[i][0],
                                    location.getCoordY() + directions[i][1]);
                            if (food.isRiped()) {
                                food.eatFood();
                            }
                        } else if (neighbors[i].getKind() == Identificator.Kind.ANIMAL) {
                            // Neighbor is animal, remove him from objectFiled
                            simLocation.updateObjectField(location.getCoordX() + directions[i][0],
                                    location.getCoordY() + directions[i][1], null);
                        }
                        // Update energy
                        energy += possibleFood.get(neighbors[i]);
                    } else {
                        int neighborIndex = simLocation.getIndex();
                        int neighborCoordX = location.getCoordX();
                        int neighborCoordY = location.getCoordY();
                        if (location.getCoordY() + directions[i][1] < 0) {
                            neighborIndex -= simLocation.getWorldWidth();
                            neighborCoordY += simLocation.getHeight();
                        } else if (location.getCoordX() + directions[i][0] < 0) {
                            neighborIndex -= 1;
                            neighborCoordX += simLocation.getWidth();
                        } else if (location.getCoordX() + directions[i][0] > simLocation.getWidth()) {
                            neighborIndex += 1;
                            neighborCoordX -= simLocation.getWidth();
                        } else if (location.getCoordY() + directions[i][1] > simLocation.getHeight()) {
                            neighborIndex += simLocation.getWorldWidth();
                            neighborCoordY -= simLocation.getHeight();
                        }
                        // different action for plant and animal
                        if (neighbors[i].getKind() == Identificator.Kind.FOOD) {
                            simLocation.getSender().sendMessage(Message.EAT, neighborCoordX, neighborCoordY,
                                    neighborIndex, null);
                            energy += possibleFood.get(neighbors[i]);
                        } else if (neighbors[i].getKind() == Identificator.Kind.ANIMAL) {
                            simLocation.getSender().sendMessage(Message.KILL, neighborCoordX, neighborCoordY,
                                    neighborIndex, null);
                            energy += possibleFood.get(neighbors[i]);
                        }
                    }
                }
                // If energy is full eat no more
                if (energy == maxEnergy) {
                    return;
                }
            }
        }
    }

    protected boolean isInBounds(int idx) {
        return location.getCoordX() + directions[idx][0] >= 0 && location.getCoordX() +
                directions[idx][0] < simLocation.getWidth() && location.getCoordY() + directions[idx][1] >= 0 &&
                location.getCoordY() + directions[idx][1] < simLocation.getHeight();
    }

    public boolean hasMatedThisRound() {
        return matedThisRound;
    }

    public void setMatedThisRound(boolean matedThisRound) {
        this.matedThisRound = matedThisRound;
    }

    public int getMaxEnergy() {
        return maxEnergy;
    }

    public int getEnergy() {
        return energy;
    }

    public Terrain[] getPossibleTerrain() {
        return possibleTerrain;
    }
}
