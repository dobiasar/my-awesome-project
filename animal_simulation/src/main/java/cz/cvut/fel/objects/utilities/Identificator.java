package cz.cvut.fel.objects.utilities;

/**
 * enum of identifiers for all kinds of objects with their string representation for communication
 */
public enum Identificator {
    BEAR("bear", Kind.ANIMAL),
    BIRD("bird", Kind.ANIMAL),
    FOX("fox", Kind.ANIMAL),
    HARE("hare", Kind.ANIMAL),
    HUNTER("hunter", Kind.HUMAN),
    APPLETREE("appletree", Kind.FOOD),
    CROPS("crops", Kind.FOOD),
    ROCK("rock", Kind.OTHER),
    HOUSE("house", Kind.OTHER),
    SHOT("shot", Kind.OTHER),
    FISH("fish", Kind.ANIMAL);

    /**
     * enum to group objects with identificator into groups with common properties
     */
    public enum Kind {
        ANIMAL, HUMAN, FOOD, OTHER;
    }

    private final String id;
    private final Kind kind;

    private Identificator(String id, Kind kind) {
        this.id = id;
        this.kind = kind;
    }

    public Kind getKind() {
        return kind;
    }

    public String getId() {
        return id;
    }

}
