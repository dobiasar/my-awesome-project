package cz.cvut.fel.objects.creatures;

import cz.cvut.fel.objects.Terrain;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

import java.util.HashMap;

/**
 * subclass of {@link Animal}
 * represent hares
 */
public class Hare extends Animal {
    public Hare(int coordX, int coordY, int fieldsPerRound, int maxEnergy, int lifeLength, SimLocation simLocation, int multiplicationRate) {
        super(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, Identificator.HARE, simLocation, multiplicationRate);
        this.possibleTerrain = new Terrain[]{Terrain.MEADOW, Terrain.FOREST};

        this.possibleFood = new HashMap<>();
        possibleFood.put(Identificator.CROPS, maxEnergy);
        possibleFood.put(Identificator.APPLETREE, maxEnergy);
    }
}
