package cz.cvut.fel.objects;

/**
 * interface for objects that are to be periodically updated
 *
 * @see cz.cvut.fel.objects.creatures.LivingCreature
 * @see cz.cvut.fel.objects.foods.Food
 * @see Shot
 */
public interface Timed {
    void newDayUpdate();
}
