package cz.cvut.fel.objects.creatures;

import cz.cvut.fel.objects.Terrain;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

import java.util.HashMap;

/**
 * subclass of {@link Animal}
 * represent birds
 */
public class Bird extends Animal {

    public Bird(int coordX, int coordY, int fieldsPerRound, int maxEnergy, int lifeLength, SimLocation simLocation, int multiplicationRate) {
        super(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, Identificator.BIRD, simLocation, multiplicationRate);
        this.possibleTerrain = new Terrain[]{Terrain.WATER, Terrain.MEADOW, Terrain.FOREST};

        this.possibleFood = new HashMap<>();
        possibleFood.put(Identificator.FISH, maxEnergy);
    }
}
