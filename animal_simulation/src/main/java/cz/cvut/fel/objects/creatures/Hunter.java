package cz.cvut.fel.objects.creatures;

import cz.cvut.fel.objects.Shot;
import cz.cvut.fel.objects.Terrain;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.distantlocation.SimLocation;

import java.io.IOException;
import java.util.HashMap;

import static java.lang.Math.max;

/**
 * subclass of {@link LivingCreature}
 * <p>
 * class to represent human (hunter)
 */
public class Hunter extends LivingCreature {

    private final int shootingRate;
    private int lastShot;

    public Hunter(int coordX, int coordY, int fieldsPerRound, int maxEnergy, int lifeLength,
                  SimLocation simLocation, int shootingRate) {
        super(coordX, coordY, fieldsPerRound, maxEnergy, lifeLength, Identificator.HUNTER, simLocation);
        this.shootingRate = shootingRate;
        this.lastShot = shootingRate;
        this.possibleTerrain = new Terrain[]{Terrain.MEADOW, Terrain.FOREST};
        this.possibleFood = new HashMap<>();
        possibleFood.put(Identificator.APPLETREE, maxEnergy / 4);
        possibleFood.put(Identificator.HOUSE, maxEnergy);
        possibleFood.put(Identificator.HARE, maxEnergy / 3);
        possibleFood.put(Identificator.FOX, maxEnergy / 2);
        possibleFood.put(Identificator.BEAR, maxEnergy);
        possibleFood.put(Identificator.FISH, maxEnergy / 3);
        possibleMoves = directions;
    }

    /**
     * method for hunter multiplication
     * if hunter is near a house he can multiply
     *
     * @throws IOException if instance is located near simulation location edges, it might face communication issues
     */
    public void newHuman() throws IOException {
        Identificator[] neighbors = simLocation.getNeighbors(location.getCoordX(), location.getCoordY());
        for (Identificator neighborId : neighbors) {
            if (neighborId == Identificator.HOUSE) {
                for (int i = 0; i < max(simLocation.getHeight(), simLocation.getWidth()); i++) {
                    for (int[] direction : possibleMoves) {
                        if (isLocationPossible(direction, i)) {
                            Hunter hunter = new Hunter(location.getCoordX() + direction[0], location.getCoordY() + direction[1], fieldsPerRound, maxEnergy, lifeLength, simLocation, shootingRate);
                            simLocation.updateObjectField(hunter.location.getCoordX(), hunter.location.getCoordY(), hunter);
                            return;
                        }
                    }
                }
            }
        }
    }

    /**
     * adding possibility to shoot
     */
    @Override
    public void move() {
        super.move();
        if (lastShot == shootingRate) {
            int idx = randomGenerator.nextInt(4);
            shoot(possibleMoves[idx]);
        }
    }

    /**
     * create new shot instance and give it momentum
     *
     * @param direction direction in which the shot will be moving
     */
    public void shoot(int[] direction) {
        // Make new instance of shot
        if (location.getCoordX() + direction[0] >= 0 && location.getCoordX() + direction[0] < simLocation.getWidth() && location.getCoordY() + direction[1] >= 0 && location.getCoordY() + direction[1] < simLocation.getHeight()) {
            Shot shot = new Shot(location.getCoordX() + direction[0], location.getCoordY() + direction[1], direction, simLocation);
            shot.resultsOfShotMove();
            lastShot = 0;
        }
    }
}
