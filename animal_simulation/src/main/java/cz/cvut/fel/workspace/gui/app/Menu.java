package cz.cvut.fel.workspace.gui.app;

import cz.cvut.fel.workspace.host.Application;

import javax.swing.*;
import java.awt.*;

/**
 * Class that graphically represents Menu
 *
 * @see JFrame
 */
public class Menu extends JFrame {

    // Buttons
    JButton playButton;
    JButton settingsButton;
    JButton exitButton;

    //Variables to keep info about amount of animals in each SimLocation
    private int bears = 7;
    private int foxes = 7;
    private int hares = 3;
    private int birds = 3;
    private int hunters = 3;
    private int appleTree = 5;
    private int crops = 4;
    private int rocks = 4;
    private int houses = 3;
    private int lifeLength = 15;
    private int maxEnergy = 10;

    private final Application app;

    /**
     * Class constructor, initializes Buttons.
     *
     * @param app - {@link Application} to which the menu is connected
     */
    public Menu(Application app) {
        super("Life in Nature");
        this.app = app;

        playButton = new JButton("PLAY");
        settingsButton = new JButton("Settings");
        exitButton = new JButton("Exit"); // Ends program

        innit();
    }

    private void innit() {

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBackground(new Color(95, 176, 33));

        //Title of panel
        Font titleFont = new Font("Arial", Font.BOLD, 30);
        JLabel title = new JLabel("LIFE IN NATURE");
        title.setForeground(new Color(255, 255, 255));
        title.setFont(titleFont);
        title.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(title);

        //Play button
        playButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(playButton);
        playButton.addActionListener(e -> {
            startSim();
            this.dispose();
        });

        // Settings button
        settingsButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(settingsButton);
        settingsButton.addActionListener(e -> new Settings(this));

        //Exit button
        exitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(exitButton);
        exitButton.addActionListener(e -> exitAll());

        this.add(panel);
    }

    private void startSim() {
        String fileContent = bears + " " + foxes + " " + hares + " " + birds + " " + hunters + " " + appleTree + " " +
                crops + " " + rocks + " " + houses + " " + lifeLength + " " + maxEnergy;
        app.setSettings(fileContent);
        app.run();
    }

    public void exitAll(){
        app.getServer().exitAfterMenu();
        System.exit(0);
    }

    public void setBears(int bears) {
        this.bears = bears;
    }

    public void setFoxes(int foxes) {
        this.foxes = foxes;
    }

    public void setHares(int hares) {
        this.hares = hares;
    }

    public void setBirds(int birds) {
        this.birds = birds;
    }

    public void setHunters(int hunters) {
        this.hunters = hunters;
    }

    public void setAppleTree(int appleTree) {
        this.appleTree = appleTree;
    }

    public void setCrops(int crops) {
        this.crops = crops;
    }

    public void setRocks(int rocks) {
        this.rocks = rocks;
    }

    public void setHouses(int houses) {
        this.houses = houses;
    }

    public void setLifeLength(int lifeLength) {
        this.lifeLength = lifeLength;
    }

    public void setMaxEnergy(int maxEnergy) {
        this.maxEnergy = maxEnergy;
    }

    public int getBears() {
        return bears;
    }

    public int getFoxes() {
        return foxes;
    }

    public int getHares() {
        return hares;
    }

    public int getBirds() {
        return birds;
    }

    public int getHunters() {
        return hunters;
    }

    public int getAppleTree() {
        return appleTree;
    }

    public int getCrops() {
        return crops;
    }

    public int getRocks() {
        return rocks;
    }

    public int getHouses() {
        return houses;
    }

    public int getLifeLength() {
        return lifeLength;
    }

    public int getMaxEnergy() {
        return maxEnergy;
    }
}
