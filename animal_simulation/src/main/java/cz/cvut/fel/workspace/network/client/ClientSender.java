package cz.cvut.fel.workspace.network.client;

import cz.cvut.fel.objects.GeneralObject;
import cz.cvut.fel.objects.foods.Food;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.distantlocation.SimLocation;
import cz.cvut.fel.workspace.network.Message;
import cz.cvut.fel.workspace.serialization.ConfigurationHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides connection for {@link SimLocation}
 */
public class ClientSender {
    protected static final Logger logger = Logger.getLogger(ClientSender.class.getName());
    protected Socket socket;
    protected PrintWriter out;
    private BufferedReader in;
    private final SimLocation myLocation;
    private Identificator neighboringObject = null;

    private Message message = null;
    private int coordX = -1;
    private int coordY = -1;
    private int nextIdx = -1;
    private String note = null;

    public ClientSender(Socket socket, SimLocation myLocation) {
        logger.log(Level.INFO, "Connection established.");
        this.socket = socket;
        this.myLocation = myLocation;
        openStreams();
        listen();
    }

    /**
     * Open socket via given port and open streams
     */
    protected void openStreams() {
        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Unable to open streams.");
        }
    }

    /**
     * Close socket and streams when simulation is ending
     */
    public void closeSocket() {
        try {
            in.close();
            out.close();
            if (!socket.isClosed()) {
                socket.close();
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "ERROR: Unable to close socket or streams.");
        }
        logger.log(Level.INFO, "Connection closed.");
    }

    /**
     * send content of file with serialization of {@link SimLocation}
     */
    private void sendCompleteData() {
        ConfigurationHandler.serializeConfigurationToFile(myLocation, this);
    }

    /**
     * decide how many parameters does the message need and parse desired parameters into sendable String
     *
     * @param newMessage            Predefined message from {@link Message}
     * @param coordX                integer coordinate
     * @param coordY                integer coordinate
     * @param neighboringLocationId integer index of location to which the message should be sent
     * @param note                  String id of identificator of the object on the field
     */
    public void sendMessage(Message newMessage, int coordX, int coordY, int neighboringLocationId, String note) {

        switch (newMessage.getArrity()) {
            case 0 -> directMessage(newMessage, null);
            case 1 -> directMessage(newMessage, note);
            case 2 -> directMessage(newMessage, (coordX + " " + coordY));
            case 3 -> directMessage(newMessage, (coordX + " " + coordY + " " + neighboringLocationId));
            default -> logger.log(Level.WARNING, "Invalid message.");
        }
    }

    /**
     * send message to server
     *
     * @param newMessage Predefined message from {@link Message}
     * @param note       String to be sent with the message
     */
    protected void directMessage(Message newMessage, String note) {
        if (newMessage.getArrity() == 0) {
            out.println((newMessage.getText()));
        } else {
            out.println((newMessage.getText() + " " + note));
        }
    }

    /**
     * Tell simulation how to react to certain message
     */
    public void react() {
        switch (message) {
            case HANDOVER:
                sendOccupation(coordX, coordY, nextIdx);
                break;
            case GATHER:
                sendCompleteData();
                break;
            case UPDATE:
                try {
                    myLocation.update();
                } catch (IOException e) {
                    logger.log(Level.SEVERE, "Could not update simulation.");
                }
                break;
            case NEIGHBOR:
                for (Identificator id : Identificator.values()) {
                    if (note.equals(id.getId())) {
                        neighboringObject = id;
                    }
                }
                break;
            case KILL:
                myLocation.updateObjectField(coordX, coordY, null);
                break;
            case EAT:
                Food food = (Food) myLocation.getObject(coordX, coordY);
                food.eatFood();
                break;
            case EXIT:
                myLocation.end();
                closeSocket();
                break;
            case START:
                receiveFile();
                break;
            default:
                logger.log(Level.WARNING, "Received invalid message: " + message + ".");
        }
    }

    /**
     * reply to "HANDOVER" message with identification of creature/food on required position
     *
     * @param coordx coordinate
     * @param coordy coordinate
     */
    private void sendOccupation(int coordx, int coordy, int backIdx) {
        String id;
        GeneralObject object = myLocation.getObject(coordx, coordy);
        if (object == null) {
            id = null;
        } else if (object.getIdentificator().getKind() == Identificator.Kind.FOOD) {
            Food food = (Food) object;
            if (!food.isRiped()) {
                id = null;
            } else {
                id = food.getIdentificator().getId();
            }
        } else {
            id = object.getIdentificator().getId();
        }
        sendMessage(Message.NEIGHBOR, 0, 0, backIdx, id);
    }

    /**
     * receiving basic settings from server
     */
    protected void decodeFile(String file) {
        int expectedNumberOfValues = 12;//number of parameters needing to be set
        String[] lines = file.split(" ");
        int[] values = new int[expectedNumberOfValues];
        for (int i = 0; i < expectedNumberOfValues; i++) {
            try {
                values[i] = Integer.parseInt(lines[i]);
            } catch (NumberFormatException e) {
                logger.log(Level.SEVERE, "Could not parse int.");
            }
        }
        myLocation.setParameters(values);
        myLocation.run();
    }

    /**
     * sets neighboring object identificator to null
     *
     * @return previous value of neighboringObject
     */
    public Identificator getNeighboringObject() {
        Identificator object = neighboringObject;
        neighboringObject = null;
        return object;
    }

    /**
     * send current state of location to server to create representation of the whole world
     */
    public void sendFile(String gameMap, String objectMap) {
        out.println(gameMap);
        out.println(objectMap);
    }

    /**
     * create new {@link Thread} that listens to server for messages and calls react method
     */
    private void listen() {
        new Thread(() -> {
            while (message != Message.EXIT && !socket.isClosed()) {
                String inputLine = null;
                try {
                    inputLine = in.readLine();
                } catch (IOException e) {
                    logger.log(Level.SEVERE, "Could not read from  stream.");
                }
                if (inputLine != null) {
                    String[] word = inputLine.split(" ");
                    for (Message newMessage : Message.values()) {
                        if (newMessage.getText().equals(word[0])) {
                            message = newMessage;
                        }
                    }
                    if (message != null) {
                        switch (message.getArrity()) {
                            case 0 -> {
                            }
                            case 2 -> {
                                nextIdx = Integer.parseInt(word[1]);
                                note = word[2];
                            }
                            case 3 -> {
                                coordX = Integer.parseInt(word[1]);
                                coordY = Integer.parseInt(word[2]);
                                nextIdx = Integer.parseInt(word[3]);
                            }
                            default -> logger.log(Level.SEVERE, "Invalid message.");
                        }
                        react();
                    }
                }
            }
        }).start();
    }

    /**
     * read from the stream and receive file with settings
     */
    public void receiveFile() {
        String line = null;
        try {
            while (line == null) {
                line = in.readLine();
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, "File received incorrectly.");
        }
        if (line == null) {
            logger.log(Level.SEVERE, "Received empty file");
        } else {
            decodeFile(line);
        }
    }
}
