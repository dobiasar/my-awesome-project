package cz.cvut.fel.workspace.gui.sim;

import cz.cvut.fel.objects.GeneralObject;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.distantlocation.SimLocation;
import cz.cvut.fel.workspace.gui.TileManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class extending {@link JPanel}, which graphically represents the part of the world corresponding with {@link
 * cz.cvut.fel.workspace.distantlocation.SimLocation}
 *
 * @see JPanel
 */
public class SimLocationGui extends JPanel {

    private final SimLocation simLocation;
    private final TileManager tileManager = new TileManager();
    private final int tileSize;

    JButton nextDayButton;
    JButton worldButton;
    JButton exitButton;

    private static final Logger logger = Logger.getLogger(SimLocationGui.class.getName());

    /**
     * Class constructor.
     *
     * @param simLocation - {@link SimLocation} which we want to graphically represent
     */
    public SimLocationGui(SimLocation simLocation) {
        this.simLocation = simLocation;
        tileSize = SimLocationGuiLauncher.TILE_SIZE;

        //Panel Settings
        this.setPreferredSize(new Dimension(tileSize * simLocation.getWidth(), tileSize *
                simLocation.getHeight()));
        this.setBackground(Color.black);
        this.setDoubleBuffered(true);

        nextDayButton = new JButton();
        nextDayButton.addActionListener(e -> simLocation.sendUpdateMessage());
        worldButton = new JButton();
        worldButton.addActionListener(e -> simLocation.sendGatherMessage());
        exitButton = new JButton();
        exitButton.addActionListener(e -> {
            simLocation.sendExitMessage();
        });

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        draw(g2);
        g2.dispose();
    }

    public SimLocation getSimLocation() {
        return simLocation;
    }

    /**
     * Draws map of the {@link SimLocation} and buttons for loading the next day and showing the whole world using
     * {@link cz.cvut.fel.workspace.gui.world.WorldGuiLauncher} (actions added in construcotr).
     *
     * @param g2 - {@link Graphics2D} to which we want to draw images
     */
    public void draw(Graphics2D g2) {
        int animalSize = ((tileSize * 2) / 3);

        //Draw terrain icons
        for (int coordX = 0; coordX < simLocation.getWidth(); coordX++) {
            for (int coordY = 0; coordY < simLocation.getHeight(); coordY++) {
                g2.drawImage(tileManager.getTile()[simLocation.getGameMap()[coordX][coordY].getNumber()],
                        coordX * tileSize, coordY * tileSize, tileSize, tileSize, null);
            }
        }

        //Draw object icons
        for (int coordX = 0; coordX < simLocation.getWidth(); coordX++) {
            for (int coordY = 0; coordY < simLocation.getHeight(); coordY++) {
                GeneralObject object = simLocation.getObject(coordX, coordY);
                if (object != null) {
                    if (object.getIdentificator().getKind() == Identificator.Kind.ANIMAL) {
                        g2.drawImage(tileManager.getTile()[tileManager.objectToNumber(object)], coordX * tileSize,
                                coordY * tileSize, animalSize, animalSize, null);
                    } else {
                        g2.drawImage(tileManager.getTile()[tileManager.objectToNumber(object)], coordX * tileSize,
                                coordY * tileSize, tileSize, tileSize, null);
                    }
                }
            }
        }

        //Making and setting next day button
        BufferedImage nextDay = null;
        try {
            nextDay = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/nextbutton.png")));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot load image");
        }
        assert nextDay != null;
        nextDayButton.setIcon(new ImageIcon(nextDay));
        this.add(nextDayButton);
        nextDayButton.setBounds(10, 5, nextDay.getWidth(), nextDay.getHeight());

        //Making and setting show-whole-world button
        BufferedImage world = null;
        try {
            world = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/worldbutton.png")));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot load image");
        }
        assert world != null;
        worldButton.setIcon(new ImageIcon(world));
        this.add(worldButton);
        worldButton.setBounds(nextDay.getWidth() + 20, 5, world.getWidth(), world.getHeight());

        //Making exit button
        BufferedImage exit = null;
        try {
            exit = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/exitbutton.png")));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot load image");
        }
        assert exit != null;
        exitButton.setIcon(new ImageIcon(exit));
        this.add(exitButton);
        exitButton.setBounds(this.getWidth() - exit.getWidth() - 10, 5, exit.getWidth(), exit.getHeight());
    }
}

