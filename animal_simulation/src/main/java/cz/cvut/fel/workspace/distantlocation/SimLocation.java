package cz.cvut.fel.workspace.distantlocation;

import cz.cvut.fel.objects.GeneralObject;
import cz.cvut.fel.objects.Terrain;
import cz.cvut.fel.objects.Timed;
import cz.cvut.fel.objects.creatures.Animal;
import cz.cvut.fel.objects.creatures.Hunter;
import cz.cvut.fel.objects.creatures.LivingCreature;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.gui.sim.SimLocationGuiLauncher;
import cz.cvut.fel.workspace.network.Message;
import cz.cvut.fel.workspace.network.client.ClientSender;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

/**
 * Application for distant computers
 */
public class SimLocation {
    //this should be a class to process the simulation itself
    //needs the update method and innit method
    //gets settings from Host...?
    //most definitely needs its gui

    private static final int WIDTH = 21;
    private static final int HEIGHT = 14;
    private static final int FISH = 2;

    private int worldWidth;
    private int worldSize;
    private int index;
    private final GeneralObject[][] objectField;
    private final int[][] directions = {{0, -1}, {-1, 0}, {1, 0}, {0, 1}};
    private final ClientSender sender;
    private final SimLocationGuiLauncher simulationGuiLauncher = new SimLocationGuiLauncher(this);
    private final Terrain[][] gameMap = new Terrain[WIDTH][HEIGHT];

    private boolean isLaunched = false;

    private int bears;
    private int foxes;
    private int hares;
    private int birds;
    private int hunters;
    private int appleTree;
    private int crops;
    private int rocks;
    private int houses;
    private int lifeLength;
    private int maxEnergy;

    public SimLocation(String ip, int port) {
        this.objectField = new GeneralObject[WIDTH][HEIGHT];
        Socket socket = null;
        try {
            socket = new Socket(ip, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.sender = new ClientSender(socket, this);
    }

    public void run() {
        SimLocationUtils.createMap(this);
        placeAll();
        isLaunched = true;
        simulationGuiLauncher.launch();
    }

    /**
     * goes through all objects on the board and updates them
     *
     * @throws IOException if some creature/food is located near simulation location edges, we might face
     *                     communication issues
     */
    public void update() throws IOException {
        // updates all simLocation
        newDay();
        simulationGuiLauncher.getSimLocationGui().repaint();
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        updateCreaturesEating();
        simulationGuiLauncher.getSimLocationGui().repaint();
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        updateCreaturesMoving();
        simulationGuiLauncher.getSimLocationGui().repaint();
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        updateCreaturesMating();
        simulationGuiLauncher.getSimLocationGui().repaint();
    }

    public void sendUpdateMessage() {
        sender.sendMessage(Message.UPDATE, 0, 0, 0, null);
    }

    public void sendExitMessage() {
        sender.sendMessage(Message.EXIT, 0, 0, 0, null);
    }

    public void sendGatherMessage() {
        sender.sendMessage(Message.GATHER, 0, 0, 0, null);
    }

    /**
     * Add day to counters of timed objects
     */
    private void newDay() {
        // Does newDayUpdate for Food, Shots, LivingCreatures
        for (GeneralObject[] generalObjects : objectField) {
            for (GeneralObject object : generalObjects) {
                // Go through all fields of the world
                if (object != null && (object.getIdentificator().getKind() == Identificator.Kind.FOOD ||
                        object.getIdentificator().getKind() == Identificator.Kind.ANIMAL ||
                        object.getIdentificator().getKind() == Identificator.Kind.HUMAN ||
                        object.getIdentificator() == Identificator.SHOT)) {
                    // If object is Timed (e.g. not Barrier) update it
                    ((Timed) object).newDayUpdate();
                }
            }
        }
    }

    /**
     * run eat method on all living creatures
     */
    private void updateCreaturesEating() {
        for (GeneralObject[] generalObjects : objectField) {
            for (GeneralObject object : generalObjects) {
                if (object != null && (object.getIdentificator().getKind() == Identificator.Kind.ANIMAL ||
                        object.getIdentificator().getKind() == Identificator.Kind.HUMAN))
                    ((LivingCreature) object).eat();
            }
        }
    }

    /**
     * run mate method on all living creatures if it is available for them
     *
     * @throws IOException if some creature/food is located near simulation location edges, we might face
     *                     communication issues
     */
    private void updateCreaturesMating() throws IOException {
        // Calls mating method for animals and newHuman method for Humans
        for (GeneralObject[] generalObjects : objectField) {
            for (GeneralObject object : generalObjects) {
                if (object != null) {
                    if (object.getIdentificator().getKind() == Identificator.Kind.ANIMAL) {
                        ((Animal) object).mate();
                    } else if (object.getIdentificator().getKind() == Identificator.Kind.HUMAN) {
                        ((Hunter) object).newHuman();
                    }
                }
            }
        }
    }

    /**
     * moves non-static creatures
     */
    private void updateCreaturesMoving() {
        // Moves all non-static creatures + makes hunters shoot in the direction they moved
        for (GeneralObject[] generalObjects : objectField) {
            for (GeneralObject object : generalObjects) {
                // If object is not static check if its Animal/Human or Fish and move it accrodingly
                if (object != null && (!object.getLocation().isStatic() && (object.getIdentificator().getKind() == Identificator.Kind.ANIMAL
                        || object.getIdentificator().getKind() == Identificator.Kind.HUMAN))) {
                    ((LivingCreature) object).move();
                }
            }
        }

    }

    public void updateObjectField(int coordX, int coordY, GeneralObject object) {
        objectField[coordX][coordY] = object;
    }

    public GeneralObject getObject(int coordX, int coordY) {
        return objectField[coordX][coordY];
    }

    public Identificator[] getNeighbors(int coordX, int coordY) {
        // Gets identificator of all neighbors of given field (if there is none -> null)
        int currCoordY;
        int currCoordX;

        Identificator[] neighbors = new Identificator[4];

        for (int i = 0; i < 4; i++) {
            currCoordX = coordX + directions[i][0];
            currCoordY = coordY + directions[i][1];
            if (currCoordY < HEIGHT && currCoordY >= 0 && currCoordX < WIDTH && currCoordX >= 0) {
                // Neighbors from current simLocation
                GeneralObject neighbor = getObject(currCoordX, currCoordY);
                if (neighbor != null) {
                    neighbors[i] = neighbor.getIdentificator();
                } else {
                    neighbors[i] = null;
                }
            } else if (currCoordX < 0 && index - 1 >= 0) {
                // Left neighbor
                sender.sendMessage(Message.COMPARE, currCoordX + WIDTH, currCoordY, index - 1, null);
                neighbors[i] = (sender.getNeighboringObject());
            } else if (currCoordX > WIDTH && index + 1 < worldSize) {
                // Right neighbor
                sender.sendMessage(Message.COMPARE, currCoordX - WIDTH, currCoordY, index + 1, null);
                neighbors[i] = (sender.getNeighboringObject());
            } else if (currCoordY < 0 && index - worldWidth >= 0) {
                sender.sendMessage(Message.COMPARE, currCoordX, currCoordY + HEIGHT, index - worldWidth, null);
                neighbors[i] = (sender.getNeighboringObject());
            } else if (currCoordY > HEIGHT && index + worldWidth < worldSize) {
                sender.sendMessage(Message.COMPARE, currCoordX, currCoordY - HEIGHT, index + worldWidth, null);
                neighbors[i] = (sender.getNeighboringObject());
            }
        }
        return neighbors;
    }

    public void end() {
        if (isLaunched) {
            simulationGuiLauncher.getFrame().dispose();
        }
    }

    private void placeAll() {
        for (int i = 0; i < bears; i++) {
            SimLocationUtils.placeObject(Identificator.BEAR, this, maxEnergy, lifeLength);
        }
        for (int i = 0; i < birds; i++) {
            SimLocationUtils.placeObject(Identificator.BIRD, this, maxEnergy, lifeLength);
        }
        for (int i = 0; i < FISH; i++) {
            SimLocationUtils.placeObject(Identificator.FISH, this, maxEnergy, lifeLength);
        }
        for (int i = 0; i < foxes; i++) {
            SimLocationUtils.placeObject(Identificator.FOX, this, maxEnergy, lifeLength);
        }
        for (int i = 0; i < hares; i++) {
            SimLocationUtils.placeObject(Identificator.HARE, this, maxEnergy, lifeLength);
        }
        for (int i = 0; i < hunters; i++) {
            SimLocationUtils.placeObject(Identificator.HUNTER, this, maxEnergy, lifeLength);
        }
        for (int i = 0; i < appleTree; i++) {
            SimLocationUtils.placeObject(Identificator.APPLETREE, this, maxEnergy, lifeLength);
        }
        for (int i = 0; i < crops; i++) {
            SimLocationUtils.placeObject(Identificator.CROPS, this, maxEnergy, lifeLength);
        }
        for (int i = 0; i < rocks; i++) {
            SimLocationUtils.placeObject(Identificator.ROCK, this, maxEnergy, lifeLength);
        }
        for (int i = 0; i < houses; i++) {
            SimLocationUtils.placeObject(Identificator.HOUSE, this, maxEnergy, lifeLength);
        }
    }

    public int getWidth() {
        return WIDTH;
    }

    public int getHeight() {
        return HEIGHT;
    }

    public int getIndex() {
        return index;
    }

    public int getWorldWidth() {
        return worldWidth;
    }

    public ClientSender getSender() {
        return sender;
    }

    public Terrain[][] getGameMap() {
        return gameMap;
    }

    /**
     * set parameters to data from server
     *
     * @param values array with parameters
     */
    public void setParameters(int[] values) {
        this.index = values[0];
        this.bears = values[1];
        this.foxes = values[2];
        this.hares = values[3];
        this.birds = values[4];
        this.hunters = values[5];
        this.appleTree = values[6];
        this.crops = values[7];
        this.rocks = values[8];
        this.houses = values[9];
        this.lifeLength = values[10];
        this.maxEnergy = values[11];
    }

}

