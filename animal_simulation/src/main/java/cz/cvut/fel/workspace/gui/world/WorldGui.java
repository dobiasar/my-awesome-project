package cz.cvut.fel.workspace.gui.world;

import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.gui.TileManager;

import javax.swing.*;
import java.awt.*;

/**
 * Class extending {@link JPanel}, which graphically represents the whole world consisting of all {@link
 * cz.cvut.fel.workspace.distantlocation.SimLocation}
 *
 * @see JPanel
 */
public class WorldGui extends JPanel {

    private final TileManager tileManager = new TileManager();

    private final int tileSize;
    private final int[][] gameMaps;
    private final Identificator[][] objectMaps;

    /**
     * Class constructor
     *
     * @param tileSize - desired size of one tile in pixels
     * @param gameMaps - 2D array containing numbers corresponding with {@link cz.cvut.fel.objects.Terrain} and their
     *                  indexes in {@link TileManager}. First index corresponds with index of
     *                  {@link cz.cvut.fel.workspace.distantlocation.SimLocation}, second index is position in map
     * @param objectMaps - 2D array containing identificators from {@link com.fasterxml.jackson.annotation.JsonTypeInfo.Id}.
     *                   First index corresponds with index of {@link cz.cvut.fel.workspace.distantlocation.SimLocation},
     *                   second index is position in map
     */
    public WorldGui(int tileSize, int[][] gameMaps, Identificator[][] objectMaps) {
        this.tileSize = tileSize;
        this.gameMaps = gameMaps;
        this.objectMaps = objectMaps;

        this.setPreferredSize(new Dimension(tileSize * 21 * 5, tileSize * 14 * 5));
        this.setBackground(Color.black);
        this.setDoubleBuffered(true);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        draw(g2);
        g2.dispose();
    }

    /**
     * Draws map of the world
     *
     * @param g2 - {@link Graphics2D} to which we want to draw images
     */
    public void draw(Graphics2D g2) {
        int animalSize = ((tileSize * 2) / 3);

        // Drawing terrains
        for (int simIndex = 0; simIndex < gameMaps.length; simIndex++) {
            for (int coordY = 0; coordY < 14; coordY++) {
                for (int coordX = 0; coordX < 21; coordX++) {
                    g2.drawImage(tileManager.getTile()[gameMaps[simIndex][21 * coordY + coordX]], coordX * tileSize
                            + 21 * tileSize * (simIndex % 5), coordY * tileSize + (simIndex / 5 ) * 14 * tileSize,
                            tileSize, tileSize, null);
                }
            }
        }

        //Drawing objects
        for (int simIndex = 0; simIndex < gameMaps.length; simIndex++) {
            for (int coordY = 0; coordY < 14; coordY++) {
                for (int coordX = 0; coordX < 21; coordX++) {
                    if (objectMaps[simIndex][coordY * 21 + coordX] != null) {
                        if (objectMaps[simIndex][coordY * 21 + coordX].getKind() == Identificator.Kind.ANIMAL) {
                            g2.drawImage(tileManager.getTile()[tileManager.identificatorToNumber(objectMaps[simIndex]
                                    [coordY * 21 + coordX])], coordX * tileSize + 21 * tileSize * (simIndex % 5),
                                    coordY * tileSize + (simIndex / 5) * 14 * tileSize, animalSize, animalSize,
                                    null);
                        } else {
                            g2.drawImage(tileManager.getTile()[tileManager.identificatorToNumber(objectMaps[simIndex]
                                    [coordY * 21 + coordX])], coordX * tileSize + 21 * tileSize * (simIndex % 5),
                                    coordY * tileSize + (simIndex / 5) * 14 * tileSize, tileSize, tileSize,
                                    null);
                        }
                    }
                }
            }
        }
    }
}
