package cz.cvut.fel.workspace.network;

/**
 * Enum of possible messages to be sent
 */
public enum Message {
    COMPARE("compare", 3),// triinary node asking for neighboring pixel
    HANDOVER("handover", 3),//trinary server asking for specific pixel
    GATHER("gather", 0),//nullar gather complete inform from location
    UPDATE("update", 0),//nullar from server trigger update method in location
    NEIGHBOR("neighbor", 2),//binary sending occupation back format "neighbor bear"
    KILL("kill", 3), //to kill animal over the boarders of location
    EAT("eat", 3), //eat food over the boarders of location
    EXIT("exit", 0), //from server to nodes -> ending sim
    START("start", 0); //from server telling SimLocation to expect message with starting data

    private final String text;
    private final int arrity;

    Message(String text, int arrity) {
        this.text = text;
        this.arrity = arrity;
    }

    public String getText() {
        return text;
    }

    public int getArrity() {
        return arrity;
    }
}
