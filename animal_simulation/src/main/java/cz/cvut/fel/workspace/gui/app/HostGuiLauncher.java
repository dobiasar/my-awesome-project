package cz.cvut.fel.workspace.gui.app;

import cz.cvut.fel.workspace.host.Application;

import javax.swing.*;

/**
 * Launcher for {@link Menu}
 */
public class HostGuiLauncher {

    // GUI SETTINGS
    public static final int WIDTH = 320;
    public static final int HEIGHT = WIDTH / 12 * 9;
    public static final int SCALE = 2;
    public static final String TITLE = "Life in Nature";


    /**
     * Initializes {@link Menu}.
     *
     * @param app - {@link Application} to which the menu is connected
     */
    public void startMenu(Application app) {
        Menu frame = new Menu(app);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setSize(900, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.pack();

    }
}