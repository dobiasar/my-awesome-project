package cz.cvut.fel.workspace.serialization;

import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.distantlocation.SimLocation;
import cz.cvut.fel.workspace.network.client.ClientSender;
import cz.cvut.fel.workspace.network.server.Server;

/**
 *
 */
public class ConfigurationHandler {
    private ConfigurationHandler(){}
    /**
     * Completely serialize configuration into provided file
     *
     * @param simLocation Object to be serialized into sadi file
     *
     */
    public static void serializeConfigurationToFile(SimLocation simLocation, ClientSender clientSender) {
        StringBuilder gameMap = new StringBuilder();
        StringBuilder objectMap = new StringBuilder();
        for (int j = 0; j < simLocation.getHeight(); j++) {
            for (int i = 0; i < simLocation.getWidth(); i++) {
                gameMap.append(simLocation.getGameMap()[i][j].getNumber() + " ");
                if(simLocation.getObject(i,j) != null) {
                    objectMap.append(simLocation.getObject(i, j).getIdentificator().getId() + " ");
                }else{
                    objectMap.append("null ");
                }
            }
        }
        clientSender.sendFile(gameMap.toString(), objectMap.toString());
    }

    /**
     * Deserialize from String
     *
     */

    public static void deserializeConfigurationFromString(String note, int myIdx, Server server) {
        String [] gameMapString = note.split(System.lineSeparator())[0].split(" ");
        String [] objectMapString = note.split(System.lineSeparator())[1].split(" ");
        if(gameMapString.length != objectMapString.length){
            System.out.println(gameMapString.length + " does not equal " + objectMapString.length );
        }
        int [] gameMapNumbers = new int[gameMapString.length];
        Identificator [] objectMap = new Identificator[objectMapString.length];
        for (int i = 0; i < gameMapString.length; i++){
            gameMapNumbers[i] = Integer.parseInt(gameMapString[i]);
            if(!objectMapString[i].equals("null")) {
                for (Identificator id : Identificator.values()) {
                    if (objectMapString[i].equals(id.getId())) {
                        objectMap[i] = id;
                    }
                }
            }else{
                objectMap[i] = null;
            }
        }
        server.getGameMaps()[myIdx] = gameMapNumbers;
        server.getObjectMaps()[myIdx] = objectMap;
    }

}