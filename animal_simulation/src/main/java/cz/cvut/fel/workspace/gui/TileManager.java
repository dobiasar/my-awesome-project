package cz.cvut.fel.workspace.gui;

import cz.cvut.fel.objects.GeneralObject;
import cz.cvut.fel.objects.Shot;
import cz.cvut.fel.objects.foods.Food;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.gui.sim.SimLocationGui;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Serves for loading images as {@link BufferedImage};
 */
public class TileManager {

    private static final Logger logger = Logger.getLogger(TileManager.class.getName());

    private final BufferedImage[] tile;

    /**
     * Class constructor.
     */
    public TileManager() {
        tile = new BufferedImage[20];
        getTileImage();
    }

    public void getTileImage() {
        try {
            tile[0] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/meadow3.png")));
            tile[1] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/forest3.png")));
            tile[2] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/water3.png")));
            tile[3] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/bear.png")));
            tile[4] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/fox.png")));
            tile[5] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/hare.png")));
            tile[6] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/bird.png")));
            tile[7] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/tree.png")));
            tile[8] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/appletree.png")));
            tile[9] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/unripedcrops.png")));
            tile[10] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/crops.png")));
            tile[11] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/rock.png")));
            tile[12] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/house.png")));
            tile[13] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/hunter.png")));
            tile[14] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/fish.png")));
            tile[15] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/shotup.png")));
            tile[16] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/shotdown.png")));
            tile[17] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/shotleft.png")));
            tile[18] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/shotright.png")));
            tile[19] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/GameIcons/void.png")));

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Unable to read from image file");
        }
    }

    /**
     * Returns index of {@link BufferedImage} in field <code> tile </code> corresponding with the given object.
     *
     * @param object Object from {@link GeneralObject}
     * @return index number of image
     */
    public int objectToNumber(GeneralObject object) {
        int number;
        switch (object.getIdentificator()) {
            case BEAR -> number = 3;
            case FOX -> number = 4;
            case HARE -> number = 5;
            case BIRD -> number = 6;
            case APPLETREE -> {
                if (((Food) object).isRiped()) {
                    number = 8;
                } else {
                    number = 7;
                }
            }
            case CROPS -> {
                if (((Food) object).isRiped()) {
                    number = 10;
                } else {
                    number = 9;
                }
            }
            case ROCK -> number = 11;
            case HOUSE -> number = 12;
            case HUNTER -> number = 13;
            case FISH -> number = 14;
            case SHOT -> {
                int[] direction = ((Shot) object).getDirection();
                if (direction[0] == 1 && direction[1] == 0) {
                    number = 18;
                } else if (direction[0] == -1 && direction[1] == 0) {
                    number = 17;
                } else if (direction[0] == 0 && direction[1] == -1) {
                    number = 15;
                } else {
                    number = 16;
                }
            }
            default -> number = 19;
        }
        return number;
    }

    /**
     * Returns index of {@link BufferedImage} in field <code> tile </code> corresponding with the given {@link Identificator}.
     *
     * @param identificator from {@link Identificator}
     * @return index number of image
     */
    public int identificatorToNumber(Identificator identificator) {
        int number;
        switch (identificator) {
            case BEAR -> number = 3;
            case FOX -> number = 4;
            case HARE -> number = 5;
            case BIRD -> number = 6;
            case APPLETREE -> number = 8;
            case CROPS -> number = 10;
            case ROCK -> number = 11;
            case HOUSE -> number = 12;
            case HUNTER -> number = 13;
            case SHOT -> number = 18;
            case FISH -> number = 14;
            default -> number = 19;
        }
        return number;
    }

    public BufferedImage[] getTile() {
        return tile;
    }
}
