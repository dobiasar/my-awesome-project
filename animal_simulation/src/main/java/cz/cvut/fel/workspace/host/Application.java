package cz.cvut.fel.workspace.host;

import cz.cvut.fel.workspace.gui.app.HostGuiLauncher;
import cz.cvut.fel.workspace.network.server.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class for initializing simulation on host computer (connection server)
 * Setting of all the parameters takes place here
 * As arguments it requires the number of computers (threads) being used in the simulation and used port
 */

public class Application {
    private static final Logger logger = Logger.getLogger(Application.class.getName());


    private final Server server;
    private final HostGuiLauncher guiLauncher = new HostGuiLauncher();
    private String settings;
    private static final int MAX_SIMS = 25;

    public Application(int port, int threadNumber) {
        ServerSocket serverSocket= null;
        try {
            serverSocket = new ServerSocket(port);
        }catch(IOException e){
            logger.log(Level.SEVERE, "Unable to open socket.");
        }
        if(threadNumber > MAX_SIMS){
            logger.log(Level.WARNING, "Number of connections is too high, only " + MAX_SIMS + " connection can and will be established.");
            threadNumber = MAX_SIMS;
        }
        server = new Server(threadNumber,serverSocket);
        server.innitConnection();
        guiLauncher.startMenu(this);
    }

    public void run() {
        if(settings!= null) {
            server.setSim(settings);
            server.startThreads();
        }
    }

    public Server getServer(){
        return server;
    }
    public void setSettings(String settings) {
        this.settings = settings;
    }
}
