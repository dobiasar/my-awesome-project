package cz.cvut.fel.workspace.network.server;

import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.gui.world.WorldGuiLauncher;
import cz.cvut.fel.workspace.network.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class handling creation of sockets and passing of arguments
 */
public class Server {

    private static final Logger logger = Logger.getLogger(Server.class.getName());

    private final int numberOfConnections;
    private final ServerConnectionHandler[] connections;
    private final ExecutorService pool;
    private final Identificator[][] objectMaps;
    private final int[][] gameMaps;
    private final ServerSocket serverSocket;

    public Server(int numberOfConnections, ServerSocket serverSocket) {
        ServerConnectionHandler.setConnections(new ServerConnectionHandler[numberOfConnections]);
        this.numberOfConnections = numberOfConnections;
        this.serverSocket = serverSocket;
        connections = new ServerConnectionHandler[numberOfConnections];
        pool = Executors.newFixedThreadPool(numberOfConnections);
        objectMaps = new Identificator[numberOfConnections][];
        gameMaps = new int[numberOfConnections][];
    }

    /**
     * opens sockets and passes them to {@link ServerConnectionHandler}
     */
    public void innitConnection() {
        try {
            logger.log(Level.INFO, "Connecting ...");
            for (int i = 0; i < numberOfConnections; i++) {
                if (!serverSocket.isClosed()) {
                    Socket socket = serverSocket.accept();
                    connections[i] = new ServerConnectionHandler(socket, i, this);
                } else {
                    break;
                }
            }
            logger.log(Level.INFO, "Connection successful.");
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not establish connection.");
        }
    }

    /**
     * passes settings to all {@link ServerConnectionHandler} and starts simulations
     *
     * @param settings String of values of parameters separated by spaces
     */
    public void setSim(String settings) {
        for (ServerConnectionHandler connection : connections) {
            connection.start(settings);
        }
    }

    /**
     * executes threads
     */
    public void startThreads() {
        for (ServerConnectionHandler connection : connections) {
            pool.execute(connection);
        }
    }

    /**
     * triggered by GATHER message
     * launches GUI for world reconstruction
     */
    public void makeWorldReconstruction() {
        WorldGuiLauncher worldGuiLauncher = new WorldGuiLauncher(gameMaps, objectMaps);
        worldGuiLauncher.launch();
    }

    public Identificator[][] getObjectMaps() {
        return objectMaps;
    }

    public int[][] getGameMaps() {
        return gameMaps;
    }

    /**
     * shuts down the threads and subsequently ends application
     */
    public void end() {
        pool.shutdown();
    }

    public void exitAfterMenu() {
        try {
            connections[0].tellEveryone(Message.EXIT);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not end simulation.");
        }
    }
}
