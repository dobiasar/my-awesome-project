package cz.cvut.fel.workspace.gui.world;

import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.workspace.gui.TileManager;

import javax.swing.*;
import java.awt.*;

/**
 * Launcher of graphical representation of the whole world consisting of {@link cz.cvut.fel.workspace.distantlocation.SimLocation}
 */
public class WorldGuiLauncher {

    private final int tileSize;
    private final int[][] gameMaps;
    private final Identificator[][] objectsMap;

    /**
     * Class constructor which computes size of one tile.
     *
     * @param gameMaps   - 2D array containing numbers corresponding with {@link cz.cvut.fel.objects.Terrain} and their
     *                   indexes in {@link TileManager}. First index corresponds with index of
     *                   {@link cz.cvut.fel.workspace.distantlocation.SimLocation}, second index is position in map
     * @param objectMaps - 2D array containing identificators from {@link com.fasterxml.jackson.annotation.JsonTypeInfo.Id}.
     *                   First index corresponds with index of {@link cz.cvut.fel.workspace.distantlocation.SimLocation},
     *                   second index is position in map
     */
    public WorldGuiLauncher(int[][] gameMaps, Identificator[][] objectMaps) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth() - 10;
        double height = screenSize.getHeight() - 10;
        tileSize = (int) Math.min(width / (5 * 21), height / (5 * 14));

        this.gameMaps = gameMaps;
        this.objectsMap = objectMaps;
    }

    /**
     * Initializes {@link JFrame} sets its parametrs and initializes and adds {@link WorldGui}.
     */
    public void launch() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setResizable(false);
        frame.setSize(tileSize * 5 * 21, tileSize * 5 * 14);
        frame.setTitle("World Map");

        WorldGui worldGui = new WorldGui(tileSize, gameMaps, objectsMap);

        frame.add(worldGui);

        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
    }

}
