package cz.cvut.fel.workspace.gui.sim;

import cz.cvut.fel.workspace.distantlocation.SimLocation;
import cz.cvut.fel.workspace.gui.app.HostGuiLauncher;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Launcher of graphical representation of one {@link cz.cvut.fel.workspace.distantlocation.SimLocation}.
 */
public class SimLocationGuiLauncher {

    private SimLocation simLocation;
    private SimLocationGui simLocationGui;
    private JFrame frame;

    protected static final int TILE_SIZE = 50;

    /**
     * Class constructor.
     *
     * @param simLocation - {@link SimLocation} which we want to portrait
     */
    public SimLocationGuiLauncher(SimLocation simLocation) {
        this.simLocation = simLocation;
    }

    /**
     * Launches {@link JFrame} and gives it correct settings, initializes {@link SimLocationGui}.
     */
    public void launch() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setResizable(false);
        frame.setSize(simLocation.getWidth() * TILE_SIZE, simLocation.getHeight() * TILE_SIZE);
        frame.setTitle("Animal Simulation");

        simLocationGui = new SimLocationGui(simLocation);

        frame.add(simLocationGui);

        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
    }

    public JFrame getFrame() {
        return frame;
    }

    public SimLocationGui getSimLocationGui() {
        return simLocationGui;
    }
}
