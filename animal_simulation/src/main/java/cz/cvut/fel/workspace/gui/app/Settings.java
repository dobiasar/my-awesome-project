package cz.cvut.fel.workspace.gui.app;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Settings {@link JFrame} of {@link Menu}.
 */
public class Settings extends JFrame {
    private Menu menu;

    //Variables
    private int bears;
    private int foxes;
    private int hares;
    private int birds;
    private int hunters;
    private int appleTree;
    private int crops;
    private int rocks;
    private int houses;
    private int lifeLength;
    private int maxEnergy;

    //Max values
    private final int max_meadow = 96;
    private final int max_forest = 183;
    private final int max_water = 24;

    JPanel panel;
    GridBagConstraints pos;

    /**
     * Class constructor, sets the visual look of this class.
     *
     * @param menu - {@link Menu} tho which these settings are connected
     */
    public Settings(Menu menu) {
        this.menu = menu;

        bears = menu.getBears();
        foxes = menu.getFoxes();
        hares = menu.getHares();
        birds = menu.getBirds();
        hunters = menu.getHunters();
        appleTree = menu.getAppleTree();
        crops = menu.getCrops();
        rocks = menu.getRocks();
        houses = menu.getHouses();
        lifeLength = menu.getLifeLength();
        maxEnergy = menu.getMaxEnergy();

        panel = new JPanel();
        panel.setBackground(new Color(95, 176, 33));
        panel.setLayout(new GridBagLayout());
        pos = new GridBagConstraints();

        innit();

        this.add(panel);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); // If it is closed like this, new amounts wont save
        this.setResizable(false);
        this.setSize(900, 600);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.pack();
    }

    private void innit() {
        Color textColor = new Color(255, 255, 255);

        //TODO:make better conditions

        // Bear
        String bearName = "Bears: ";
        JLabel bearLabel = new JLabel(bearName + bears);
        bearLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 0;
        panel.add(bearLabel, pos);

        JButton bearPlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 0;
        panel.add(bearPlus, pos);
        bearPlus.addActionListener(e -> {
            if (bears + foxes + hares + birds + hunters + appleTree + crops + houses + rocks + 1 <= max_forest +
                    max_meadow) {
                bears++;
                bearLabel.setText(bearName + bears);
            }
        });

        JButton bearMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 0;
        panel.add(bearMinus, pos);
        bearMinus.addActionListener(e -> {
            if (bears > 0) {
                bears--;
                bearLabel.setText(bearName + bears);
            }
        });

        //Fox
        String foxName = "Foxes: ";
        JLabel foxLabel = new JLabel(foxName + foxes);
        foxLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 1;
        panel.add(foxLabel, pos);

        JButton foxPlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 1;
        panel.add(foxPlus, pos);
        foxPlus.addActionListener(e -> {
            if (bears + foxes + hares + birds + hunters + appleTree + crops + houses + rocks + 1 <= max_forest +
                    max_meadow) {
                foxes++;
                foxLabel.setText(foxName + foxes);
            }
        });

        JButton foxMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 1;
        panel.add(foxMinus, pos);
        foxMinus.addActionListener(e -> {
            if (foxes > 0) {
                foxes--;
                foxLabel.setText(foxName + foxes);
            }
        });

        //Hare
        String hareText = "Hares: ";
        JLabel hareLabel = new JLabel(hareText + hares);
        hareLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 2;
        panel.add(hareLabel, pos);

        JButton harePlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 2;
        panel.add(harePlus, pos);
        harePlus.addActionListener(e -> {
            if (bears + foxes + hares + birds + hunters + appleTree + crops + houses + rocks + 1 <= max_forest
                    + max_meadow) {
                hares++;
                hareLabel.setText(hareText + hares);
            }
        });

        JButton hareMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 2;
        panel.add(hareMinus, pos);
        hareMinus.addActionListener(e -> {
            if (hares > 0) {
                hares--;
                hareLabel.setText(hareText + hares);
            }
        });

        //Bird
        String birdName = "Birds: ";
        JLabel birdLabel = new JLabel(birdName + birds);
        birdLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 3;
        panel.add(birdLabel, pos);

        JButton birdPlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 3;
        panel.add(birdPlus, pos);
        birdPlus.addActionListener(e -> {
            if (bears + foxes + hares + birds + hunters + appleTree + crops + houses + rocks + 3 <= max_forest +
                    max_meadow + max_water) {
                birds++;
                birdLabel.setText(birdName + birds);
            }
        });

        JButton birdMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 3;
        panel.add(birdMinus, pos);
        birdMinus.addActionListener(e -> {
            if (birds > 0) {
                birds--;
                birdLabel.setText(birdName + birds);
            }
        });

        //Hunters
        String hunterName = "Hunters: ";
        JLabel hunterLabel = new JLabel(hunterName + hunters);
        hunterLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 4;
        panel.add(hunterLabel, pos);

        JButton hunterPlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 4;
        panel.add(hunterPlus, pos);
        hunterPlus.addActionListener(e -> {
            if (bears + foxes + hares + birds + hunters + appleTree + crops + houses + rocks + 1 <= max_forest
                    + max_meadow) {
                hunters++;
                hunterLabel.setText(hunterName + hunters);
            }
        });

        JButton hunterMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 4;
        panel.add(hunterMinus, pos);
        hunterMinus.addActionListener(e -> {
            if (hunters > 0) {
                hunters--;
                hunterLabel.setText(hunterName + hunters);
            }
        });

        //AppleTrees
        String appleName = "Apple trees: ";
        JLabel appleLabel = new JLabel(appleName + appleTree);
        appleLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 5;
        panel.add(appleLabel, pos);

        JButton applePlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 5;
        panel.add(applePlus, pos);
        applePlus.addActionListener(e -> {
            if (bears + foxes + hares + birds + hunters + rocks + appleTree + crops <= max_meadow) {
                appleTree++;
                appleLabel.setText(appleName + appleTree);
            }
        });

        JButton appleMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 5;
        panel.add(appleMinus, pos);
        appleMinus.addActionListener(e -> {
            if (appleTree > 0) {
                appleTree--;
                appleLabel.setText(appleName + appleTree);
            }
        });

        //Crops
        String cropsName = "Crops: ";
        JLabel cropsLabel = new JLabel(cropsName + crops);
        cropsLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 6;
        panel.add(cropsLabel, pos);

        JButton cropsPlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 6;
        panel.add(cropsPlus, pos);
        cropsPlus.addActionListener(e -> {
            if (bears + foxes + hares + birds + hunters + rocks + appleTree + crops <= max_meadow) {
                crops++;
                cropsLabel.setText(cropsName + crops);
            }
        });

        JButton cropsMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 6;
        panel.add(cropsMinus, pos);
        cropsMinus.addActionListener(e -> {
            if (crops > 0) {
                crops--;
                cropsLabel.setText(cropsName + crops);
            }
        });
        //Rocks
        String rocksName = "Rocks: ";
        JLabel rocksLabel = new JLabel(rocksName + rocks);
        rocksLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 7;
        panel.add(rocksLabel, pos);

        JButton rocksPlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 7;
        panel.add(rocksPlus, pos);
        rocksPlus.addActionListener(e -> {
            if (bears + foxes + hares + birds + hunters + appleTree + crops + houses + rocks + 1 <= max_forest
                    + max_meadow) {
                rocks++;
                rocksLabel.setText(rocksName + rocks);
            }
        });

        JButton rocksMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 7;
        panel.add(rocksMinus, pos);
        rocksMinus.addActionListener(e -> {
            if (rocks > 0) {
                rocks--;
                rocksLabel.setText(rocksName + rocks);
            }
        });

        //houses
        String housesName = "Houses: ";
        JLabel housesLabel = new JLabel(housesName + houses);
        housesLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 8;
        panel.add(housesLabel, pos);

        JButton housesPlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 8;
        panel.add(housesPlus, pos);
        housesPlus.addActionListener(e -> {
            if (bears + foxes + hares + birds + hunters + houses + rocks + 1 <= max_forest) {
                houses++;
                housesLabel.setText(housesName + houses);
            }
        });
        JButton housesMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 8;
        panel.add(housesMinus, pos);
        housesMinus.addActionListener(e -> {
            if (houses > 0) {
                houses--;
                housesLabel.setText(housesName + houses);
            }
        });

        //Lifelength
        String lifeName = "Length of life: ";
        JLabel lifeLabel = new JLabel(lifeName + lifeLength);
        lifeLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 9;
        panel.add(lifeLabel, pos);

        JButton lifePlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 9;
        panel.add(lifePlus, pos);
        lifePlus.addActionListener(e -> {
            lifeLength++;
            lifeLabel.setText(lifeName + lifeLength);

        });

        JButton lifeMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 9;
        panel.add(lifeMinus, pos);
        lifeMinus.addActionListener(e -> {
            if (lifeLength > 3) {
                lifeLength--;
                lifeLabel.setText(lifeName + lifeLength);
            }
        });

        //MaxEnergy
        String energyName = "Maximum energy: ";
        JLabel energyLabel = new JLabel(energyName + maxEnergy);
        energyLabel.setForeground(textColor);
        pos.gridx = 0;
        pos.gridy = 10;
        panel.add(energyLabel, pos);

        JButton energyPlus = new JButton("+");
        pos.gridx = 1;
        pos.gridy = 10;
        panel.add(energyPlus, pos);
        energyPlus.addActionListener(e -> {
            maxEnergy++;
            energyLabel.setText(energyName + maxEnergy);

        });

        JButton energyMinus = new JButton("-");
        pos.gridx = 2;
        pos.gridy = 10;
        panel.add(energyMinus, pos);
        energyMinus.addActionListener(e -> {
            if (maxEnergy > 3) {
                maxEnergy--;
                energyLabel.setText(energyName + maxEnergy);
            }
        });

        JLabel saveInfo = new JLabel("CLICK TO SAVE SETTINGS");
        saveInfo.setForeground(new Color(196, 73, 5));
        pos.gridx = 0;
        pos.gridy = 11;
        panel.add(saveInfo, pos);

        //Save button
        JButton saveButton = new JButton("SAVE");
        pos.fill = GridBagConstraints.HORIZONTAL;
        pos.gridwidth = 3;
        pos.gridx = 0;
        pos.gridy = 12;
        panel.add(saveButton, pos);
        saveButton.addActionListener(this::actionPerformed);

    }


    private void actionPerformed(ActionEvent e) {
        menu.setBears(bears);
        menu.setFoxes(foxes);
        menu.setHares(hares);
        menu.setBirds(birds);
        menu.setHunters(hunters);
        menu.setAppleTree(appleTree);
        menu.setCrops(crops);
        menu.setRocks(rocks);
        menu.setHouses(houses);
        menu.setLifeLength(lifeLength);
        menu.setMaxEnergy(maxEnergy);
        this.dispose();
    }
}
