package cz.cvut.fel.workspace.network.server;

import cz.cvut.fel.workspace.network.Message;
import cz.cvut.fel.workspace.serialization.ConfigurationHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

/**
 * Class handling connection to one of the distant location.
 * Implements{@link Runnable}
 */
public class ServerConnectionHandler implements Runnable {

    private static final Logger logger = Logger.getLogger(ServerConnectionHandler.class.getName());
    private static ServerConnectionHandler[] connections;

    private final int idx;
    private final Socket socket;
    private final Server server;
    private boolean isExit = false;

    private final BufferedReader in;
    private final PrintWriter out;

    public ServerConnectionHandler(Socket clientSocket, int idx, Server server) throws IOException {
        this.socket = clientSocket;
        this.idx = idx;
        this.server = server;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
        connections[idx] = this;
    }

    /**
     * When the play button is pressed it triggers this method which sends START message and settings to the location
     *
     * @param setting String with values of parameters separated by spaces
     */
    public void start(String setting) {
        sendMessage(Message.START, 0, 0, 0, null);
        sendSettings(setting);
    }

    @Override
    public void run() {
        try {
            while (!isExit && !socket.isClosed()) {
                listen();
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Did not receive message.");
        } finally {
            closeSocket();
            if (idx == connections.length - 1) {
                server.end();
            }
        }
    }

    /**
     * Close streams and sockets
     */
    private void closeSocket() {
        out.close();
        try {
            in.close();
            socket.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not close socket or streams.");
        }

    }

    /**
     * Decide how many parameters does the message need and parse desired parameters into sendable String
     *
     * @param message               Predefined message from {@link Message}
     * @param coordX                integer coordinate
     * @param coordY                integer coordinate
     * @param neighboringLocationId integer index of location to which the message should be sent
     * @param note                  String id of identificator of the object on the field
     */
    private void sendMessage(Message message, int coordX, int coordY, int neighboringLocationId, String note) {

        switch (message.getArrity()) {
            case 0 -> directMessage(message, null);
            case 2 -> directMessage(message, (neighboringLocationId + " " + note));
            case 3 -> directMessage(message, (coordX + " " + coordY + " " + neighboringLocationId));
            default -> logger.log(Level.WARNING, "Invalid message.");
        }
    }

    /**
     * send message to the location
     *
     * @param message Predefined message from {@link Message}
     * @param note    String to be sent with the message
     */
    private void directMessage(Message message, String note) {
        if (message.getArrity() == 0) {
            out.println((message.getText()));
        } else {
            out.println((message.getText() + " " + note));
        }
    }

    /**
     * Tell provider how to react to certain message
     */
    public void react(Message message, String note) throws IOException {
        switch (message) {
            case EXIT -> tellEveryone(message);
            case COMPARE -> {
                String[] values = note.split(" ");
                connections[Integer.parseInt(values[2])].sendMessage(Message.HANDOVER, Integer.parseInt(values[0]),
                        Integer.parseInt(values[1]), idx, null);
            }
            case EAT -> {
                String[] values = note.split(" ");
                connections[Integer.parseInt(values[2])].sendMessage(Message.EAT, Integer.parseInt(values[0]),
                        Integer.parseInt(values[1]), idx, null);
            }
            case KILL -> {
                String[] values = note.split(" ");
                connections[Integer.parseInt(values[2])].sendMessage(Message.KILL, Integer.parseInt(values[0]),
                        Integer.parseInt(values[1]), idx, null);
            }
            case UPDATE -> tellEveryone(Message.UPDATE);
            case NEIGHBOR -> sendMessage(Message.NEIGHBOR, 0, 0, Integer.parseInt(note.split(" ")[0]), note.split(" ")[1]);
            case GATHER -> tellEveryone(Message.GATHER);
            default -> logger.log(Level.WARNING, "Unsupported message.");
        }

    }

    /**
     * when not sending message, wait for message
     *
     * @throws IOException when reading from input stream exception might be thrown
     */
    public void listen() throws IOException {
        while (!in.ready() && !isExit) {
            try {
                sleep(50);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        String inputLine = in.readLine();
        if (inputLine != null) {
            String[] word = inputLine.split(" ");
            Message myMessage = null;
            for (Message message : Message.values()) {
                if (message.getText().equals(word[0])) {
                    myMessage = message;
                }
            }
            if (myMessage != null) {
                switch (myMessage.getArrity()) {
                    case 0 -> react(myMessage, null);
                    case 1 -> react(myMessage, word[1]);
                    case 2 -> react(myMessage, (word[1] + " " + word[2]));
                    case 3 -> react(myMessage, (word[1] + " " + word[2]) + " " + word[3]);
                    default -> logger.log(Level.SEVERE, "Invalid message.");
                }
            }
        }
    }

    private void decodeFile(String file) {
        ConfigurationHandler.deserializeConfigurationFromString(file, idx, server);
    }

    private void sendSettings(String text) {
        if (text != null) {
            out.println(idx + " " + text);
        } else {
            logger.log(Level.SEVERE, "Attempting to sent empty file.");
        }
    }

    /**
     * get ready to receive file from SimLocation
     */
    public void receiveFile() throws IOException {
        String line;
        line = in.readLine();
        String file = line + System.lineSeparator() + in.readLine();
        decodeFile(file);
        if (idx == connections.length - 1) {
            server.makeWorldReconstruction();
        }
    }

    /**
     * send message to every location
     *
     * @param message Predefined message from {@link Message}
     * @throws IOException when reading from input stream exception might be thrown
     */
    public void tellEveryone(Message message) throws IOException {
        for (ServerConnectionHandler connection : connections) {
            connection.sendMessage(message, 0, 0, 0, null);
            if (message == Message.GATHER) {
                connection.receiveFile();
            } else if (message == Message.EXIT) {
                connection.isTimeToExit();
            }
        }
    }

    public void isTimeToExit() {
        this.isExit = true;
    }

    public static void setConnections(ServerConnectionHandler[] newConnections) {
        connections = newConnections;
    }
}
