package cz.cvut.fel.workspace.distantlocation;

import cz.cvut.fel.objects.Barrier;
import cz.cvut.fel.objects.Terrain;
import cz.cvut.fel.objects.creatures.*;
import cz.cvut.fel.objects.foods.Food;
import cz.cvut.fel.objects.utilities.Identificator;
import cz.cvut.fel.objects.utilities.Location;

import java.util.Random;

public class SimLocationUtils {
    private static final Random random = new Random();


    private static final int WIDTH = 21;
    private static final int HEIGHT = 14;
    private static final int RESPAWN_RATE = 3;

    private SimLocationUtils() {
    }

    public static void placeObject(Identificator object, SimLocation myLocation, int maxEnergy, int lifeLenght) {
        if (object == null) {
            return;
        } else if (object.getKind() == Identificator.Kind.ANIMAL || object.getKind() == Identificator.Kind.HUMAN) {
            placeCreature(object, maxEnergy, lifeLenght, myLocation);
        } else if (object.getKind() == Identificator.Kind.FOOD) {
            boolean isPlaced = false;
            while (!isPlaced) {
                int coordx = random.nextInt(WIDTH);
                int coordy = random.nextInt(HEIGHT);
                if (myLocation.getObject(coordx, coordy) == null && myLocation.getGameMap()[coordx][coordy] !=
                        Terrain.WATER && myLocation.getGameMap()[coordx][coordy] != Terrain.FOREST) {
                    myLocation.updateObjectField(coordx, coordy, new Food(coordx, coordy, RESPAWN_RATE, myLocation, object));
                    isPlaced = true;
                }
            }
        } else {
            boolean isPlaced = false;
            while (!isPlaced) {
                int coordx = random.nextInt(WIDTH);
                int coordy = random.nextInt(HEIGHT);
                if (myLocation.getObject(coordx, coordy) == null && myLocation.getGameMap()[coordx][coordy] != Terrain.WATER) {
                    myLocation.updateObjectField(coordx, coordy, new Barrier(new Location(coordx, coordy, true), myLocation, object));
                    isPlaced = true;
                }
            }
        }
    }

    private static void placeCreature(Identificator id, int maxEnergy, int lifeLength, SimLocation myLocation) {
        LivingCreature sampleCreature;
        switch (id) {
            case FISH -> {
                sampleCreature = new Fish(0, 0, 1, maxEnergy, lifeLength, myLocation, 2);
            }
            case FOX -> {
                sampleCreature = new Fox(0, 0, 1, maxEnergy, lifeLength, myLocation, 2);
            }
            case BIRD -> {
                sampleCreature = new Bird(0, 0, 2, maxEnergy, lifeLength, myLocation, 3);
            }
            case BEAR -> {
                sampleCreature = new Bear(0, 0, 1, maxEnergy, lifeLength, myLocation, 1);
            }
            case HARE -> {
                sampleCreature = new Hare(0, 0, 1, maxEnergy, lifeLength, myLocation, 3);
            }
            case HUNTER -> {
                sampleCreature = new Hunter(0, 0, 1, maxEnergy, 2 * lifeLength, myLocation, 4);
            }
            default -> {
                sampleCreature = null;
            }
        }
        if (sampleCreature != null) {
            boolean isPlaced = false;
            while (!isPlaced) {
                int coordx = random.nextInt(WIDTH);
                int coordy = random.nextInt(HEIGHT);
                if (myLocation.getObject(coordx, coordy) == null && ((sampleCreature.getPossibleTerrain().length == 2 &&
                        (myLocation.getGameMap()[coordx][coordy] == sampleCreature.getPossibleTerrain()[0] ||
                                myLocation.getGameMap()[coordx][coordy] == sampleCreature.getPossibleTerrain()[1])) ||
                        (sampleCreature.getPossibleTerrain().length == 1 && myLocation.getGameMap()[coordx][coordy] ==
                                sampleCreature.getPossibleTerrain()[0]) || sampleCreature.getPossibleTerrain().length == 3)) {
                    sampleCreature.getLocation().setCoordX(coordx);
                    sampleCreature.getLocation().setCoordY(coordy);
                    myLocation.updateObjectField(coordx, coordy, sampleCreature);
                    isPlaced = true;
                }
            }
        }
    }

    public static void createMap(SimLocation myLocation) {
        for (int coordY = 0; coordY < HEIGHT; coordY++) {
            for (int coordX = 0; coordX < WIDTH; coordX++) {
                if (coordX * coordY < 100 && coordX + coordY > 7) {
                    myLocation.getGameMap()[coordX][coordY] = Terrain.FOREST;
                } else {
                    if (coordY * coordX + coordX / 2 < 176) {
                        myLocation.getGameMap()[coordX][coordY] = Terrain.MEADOW;
                    } else {
                        myLocation.getGameMap()[coordX][coordY] = Terrain.WATER;
                    }
                }
            }
        }
    }
}
