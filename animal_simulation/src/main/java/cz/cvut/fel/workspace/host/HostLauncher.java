package cz.cvut.fel.workspace.host;

/**
 * Launch simulation host
 *
 */
public class HostLauncher {
    public static void main(String[] argv) {

        Application app = new Application(6666, 2);
        app.run();
    }
}
